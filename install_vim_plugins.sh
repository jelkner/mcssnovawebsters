mkdir -p ~/.vim/pack/tpope/start
git clone https://github.com/tpope/vim-fugitive.git ~/.vim/pack/tpope/start/vim-fugitive
vim -u NONE -c "helptags fugitive/doc" -c q
