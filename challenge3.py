from gasp import *

begin_graphics()
x = 5
y = 5
c = Circle((x, y), 5)

while x < 640:
    x += 4
    y += 3
    move_to(c, (x, y))
    sleep(0.02)
end_graphics()
