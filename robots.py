from gasp import *
from random import randint


def place_player():
    global player_x, player_y, player_shape

    player_x =  randint(0, 63)
    player_y =  randint(0, 47)
    player_shape = Circle((10 * player_x + 5, 10 * player_y + 5), 5)


def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    # Use vim keys for left, down, up, and right
    if key == 'h' and player_x > 0:
        player_x -= 1
    elif key == 'j' and player_y > 0:
        player_y -= 1
    elif key == 'k' and player_y < 47:
        player_y += 1
    elif key == 'l' and player_x < 63:
        player_x += 1
    # Custom diagonal keys for laptops without numeric keypad
    elif key == 'u':                   # move left and up
        if player_x > 0:
            player_x -= 1
        if player_y < 47:
            player_y += 1
    elif key == 'i':                   # move right and up
        if player_x < 63:
            player_x += 1
        if player_y < 47:
            player_y += 1
    elif key == 'n':                   # move left and down 
        if player_x > 0:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
    elif key == 'm':                   # move right and down 
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1

    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))
    move_robot()


def place_robot():
    global robot_x, robot_y, robot_shape

    robot_x =  randint(0, 63)
    robot_y =  randint(0, 47)
    robot_shape = Box((10 * robot_x, 10 * robot_y), 10, 10)


def move_robot():
    global robot_x, robot_y, robot_shape, player_x, player_y

    # print(f"robot_x: {robot_x}  player_x: {player_x}")
    # if robot_x > player_x:
    #    robot_x += 1
    print(f"robot_x: {robot_x}")
    if robot_x > 63:
        robot_x = 0
    else:
        robot_x += 1

    move_to(robot_shape, (10 * robot_x, 10 * robot_y))


begin_graphics()
finished = False

place_player()
place_robot()

while not finished:
    move_player()

end_graphics()
